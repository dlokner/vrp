from typing import Tuple, List
import math


class Customer:
    def __init__(self):
        self.number: int = 0
        self.coordinate: Tuple[int, int] = (0, 0)
        self.demand: int = 0
        self.s_time: int = 0
        self.e_time: int = 0
        self.service_duration: int = 0

    @staticmethod
    def create_customer(i: int, x: int, y: int, q: int, e: int, l: int, d: int):
        customer = Customer()
        customer.number = i
        customer.coordinate = (x, y)
        customer.demand = q
        customer.s_time = e
        customer.e_time = l
        customer.service_duration = d
        return customer

    def __str__(self):
        return '\t{}\t{}\t{}\t{}\t{}\t{}\t{}'.format(
            self.number,
            self.coordinate[0], self.coordinate[1],
            self.demand,
            self.s_time, self.e_time, self.service_duration
        )

    def distance_to(self, other) -> float:
        s_point = self.coordinate
        d_point = other.coordinate
        distance = math.sqrt((s_point[0] - d_point[0]) ** 2 + (s_point[1] - d_point[1]) ** 2)
        return distance


class VehicleFleet:
    def __init__(self, k: int, q: int):
        self.number = k
        self.capacity = q

    def __str__(self):
        return '\t{}\t{}'.format(self.number, self.capacity)


class SolomonsInstance:
    def __init__(self, name: str, fleet: VehicleFleet, customers: List[Customer]):
        self.name: str = name
        self.vehicle_fleet: VehicleFleet = fleet
        self.customers: List[Customer] = customers

    def __str__(self):
        builder = self.name + '\n'*2
        builder += 'VEHICLE\nNUMBER\tCAPACITY\n' + str(self.vehicle_fleet) + '\n'*2
        builder += 'CUSTOMER\nCUST NO.\tXCOORD.\tYCOORD.\tDEMAND\tREADY TIME\tDUE DATE\tSERVICE TIME\n'
        for c in self.customers:
            builder += str(c) + '\n'
        return builder[:-1]


class Route:
    def __init__(self, customers: List[Customer], name: str = ''):
        self.name = name
        self.customers = customers

    def __str__(self):
        builder = 'Route {}:'.format(self.name)
        for customer in self.customers:
            builder += ' {}'.format(customer.number)
        return builder

    def calc_distance(self, depot: Customer) -> float:
        current: Customer = depot
        distance: float = 0.0
        for customer in self.customers:
            distance += current.distance_to(customer)
            current = customer
        distance += current.distance_to(depot)
        return distance

    def calc_route_capacity(self):
        capacity = 0.0
        for c in self.customers:
            capacity += c.demand
        return capacity

    def simplify(self) -> List[int]:
        simplified_route: List[int] = []
        for customer in self.customers:
            simplified_route.append(customer.number)
        return simplified_route


class VrpSolution:
    def __init__(self, routes: List[Route], fitness: float = 0.0):
        self.routes = routes
        self.fitness = fitness

    def simplify(self) -> List[List[int]]:
        simplified_solution: List[List[int]] = []
        for route in self.routes:
            simplified_solution.append(route.simplify())
        return simplified_solution

    def __str__(self):
        builder = ''
        for route in self.routes:
            builder += str(route) + '\n'
        builder += str(self.fitness)
        return builder
