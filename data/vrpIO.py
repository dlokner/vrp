from typing import List
from data.parser import VrpParser, VrpRoutesParser
from data.data_struct import SolomonsInstance, VrpSolution


def read_vrp_instance(filename: str) -> SolomonsInstance:
    file = open(filename)
    lines = file.readlines()
    file.close()
    parser = VrpParser(lines)
    return parser.instance


def write_vrp_instance(filename: str, parameter: str, instance: SolomonsInstance):
    file = open(filename, parameter)
    file.write(str(instance))
    file.close()


def read_simplified_vrp_solution(filename: str) -> List[List[int]]:
    file = open(filename)
    lines = file.readlines()
    file.close()
    parser = VrpRoutesParser(lines)
    return parser.fetch_simplified()


def simplified_solution_to_str(solution: List[List[int]]) -> str:
    builder = ''
    for index, route in enumerate(solution):
        builder += 'Route {}: '.format(index)
        for customer in route:
            builder += '{} '.format(customer)
        builder = builder[:-1] + '\n'
    return builder


def write_vrp_simplified_solution(filename: str, parameter: str, solution: List[List[int]]):
    file = open(filename, parameter)
    file.write(simplified_solution_to_str(solution))
    file.close()


def write_vrp_solution(filename: str, parameter: str, solution: VrpSolution):
    file = open(filename, parameter)
    file.write(str(solution))
    file.close()
