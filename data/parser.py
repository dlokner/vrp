from typing import List, Dict, Tuple
from data.data_struct import VehicleFleet, Customer, SolomonsInstance
import re


class VrpParser:

    def __init__(self, lines: List[str]):
        self.lines = lines
        fleet_input = self.extract_vehicle_info()
        customer_input = self.extract_customer_info()

        name = self.extract_name()
        fleet = self.create_fleet(fleet_input)
        customers = self.create_customer_list(customer_input)

        self.instance = SolomonsInstance(name, fleet, customers)

    def extract_vehicle_info(self) -> str or None:
        index = self.line_startswith('VEHICLE')
        if index != -1 and len(self.lines) - 1 > index + 2:
            return self.lines[index + 2]
        else:
            return None

    def extract_customer_info(self) -> List[str] or None:
        index = self.line_startswith('CUSTOMER') + 2
        customers = []

        for i in range(index, len(self.lines)):
            line = self.lines[i].strip()
            if line:
                customers.append(line)
        return customers if len(customers) else None

    def extract_name(self):
        if not self.lines:
            raise ValueError()
        return self.lines[0].strip()

    def line_startswith(self, prefix):
        for i in range(len(self.lines)):
            if self.lines[i].startswith(prefix):
                return i
        return -1

    @staticmethod
    def create_fleet(fleet_input: str) -> VehicleFleet:
        fleet_input = fleet_input.strip()
        split = re.split('\\s+', fleet_input)
        if len(split) != 2:
            raise ValueError()
        return VehicleFleet(int(split[0]), int(split[1]))

    @staticmethod
    def create_customer_list(customer_inputs) -> List[Customer]:
        customers = []
        for customer_input in customer_inputs:
            customer = VrpParser.create_customer(customer_input)
            customers.append(customer)
        return customers

    @staticmethod
    def create_customer(customer_input) -> Customer:
        customer_input = customer_input.strip()
        split = re.split('\\s+', customer_input)
        if len(split) != 7:
            raise ValueError()
        return Customer.create_customer(
            int(split[0]), int(split[1]), int(split[2]), int(split[3]), int(split[4]), int(split[5]), int(split[6])
        )


class VrpRoutesParser:

    def __init__(self, lines: List[str]):
        self.routes: Dict[str, List[int]] = {}
        self.parse(lines)

    def parse(self, lines) -> None:
        for line in lines:
            line = line.strip()
            if line.startswith('Route'):
                name, customers = VrpRoutesParser.extract_route_info(line)
                self.routes.update({name: customers})

    def fetch_simplified(self):
        simplified_routes: List[List[int]] = []
        for route in self.routes.values():
            simplified_routes.append(route)
        return simplified_routes

    @staticmethod
    def extract_route_info(line) -> Tuple[str, List[int]]:
        split_line = re.split('\\s*:\\s*', line.strip())
        if len(split_line) != 2:
            raise ValueError('Invalid format on line: {}'.format(line))
        name = split_line[0]
        customers = VrpRoutesParser.extract_customers(split_line[1])
        return name, customers

    @staticmethod
    def extract_customers(customers_str: str) -> List[int]:
        customers_list = re.split('\\s+', customers_str.strip())
        customers: List[int] = []
        for customer_str in customers_list:
            c = int(customer_str)
            customers.append(c)
        return customers
