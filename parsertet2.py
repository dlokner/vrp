from data.vrpIO import *
from simulator.vrp_simulator import VrpSimulator

from algorithms.clarke_wright import ClarkWrightAlgorithm
from algorithms.genetic_algorithms.real_coded_ga import FloatingPointAlgorithm

instance = read_vrp_instance('files/RC208.txt')
simulator = VrpSimulator(instance)

algorithm = ClarkWrightAlgorithm(simulator)
solution = algorithm.solve()
write_vrp_solution('files/clarke.txt', 'w', solution)

algorithm = FloatingPointAlgorithm(simulator)
solution = algorithm.solve()
write_vrp_solution('files/floating_point.txt', 'w', solution)


# c101
# routes = [
#     [81, 78, 76, 71, 70, 73, 77, 79, 80],
#     [57, 55, 54, 53, 56, 58, 60, 59],
#     [98, 96, 95, 94, 92, 93, 97, 100, 99],
#     [32, 33, 31, 35, 37, 38, 39, 36, 34],
#     [13, 17, 18, 19, 15, 16, 14, 12],
#     [90, 87, 86, 83, 82, 84, 85, 88, 89, 91],
#     [43, 42, 41, 40, 44, 46, 45, 48, 51, 50, 52, 49, 47],
#     [67, 65, 63, 62, 74, 72, 61, 64, 68, 66, 69],
#     [5, 3, 7, 8, 10, 11, 9, 6, 4, 2, 1, 75],
#     [20, 24, 25, 27, 29, 30, 28, 26, 23, 22, 21]
# ]
# simulator.simulate(routes)
#
# write_vrp_solution('solution.txt', 'w', simulator.best_solution)
# write_vrp_simplified_solution('simplified_solution.txt', 'w', routes)
#
# print(simplified_solution_to_str(read_simplified_vrp_solution('simplified_solution.txt')))
