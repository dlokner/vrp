from data.vrpIO import *
from simulator.vrp_simulator import VrpSimulator
from algorithms.genetic_algorithms.matrix_encoding import GeneticAlgorithmMatrix


instance = read_vrp_instance('files/C101.txt')
simulator = VrpSimulator(instance)
algorithm = GeneticAlgorithmMatrix(simulator)
solution = algorithm.solve()
list_up = []
list_down = []
print(solution)

for route in solution.routes:
    if len(route.customers)>0:
        list_down.append(len(route.customers))
        for cust in route.customers:
            list_up.append(cust.number)
print(len(list_up))


write_vrp_solution('solution_matrix.txt', 'w', solution)