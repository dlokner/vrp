from typing import List
from abc import ABC
from collections import namedtuple
import math

from algorithms.ga_utilities.structs import Chromosome, Population
from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.ga_utilities.selections import AbstractSelection, KTournamentSelection
from algorithms.ga_utilities.mutations import AbstractMutation, SwapGenesMutation, AddRandomMutation, RandomMutation
from algorithms.ga_utilities.crossovers import AbstractCrossover, WholeArithmeticCrossover, BLXAlphaCrossover, SBXCrossover

from data.data_struct import VrpSolution
from simulator.vrp_simulator import VrpSimulator


def remove_empty(solution: List[List[int]]) -> List[List[int]]:
    return [l for l in solution if l]


# values in range [0, 1]
class FloatingPointAlgorithm(AbstractAlgorithm):
    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.problem = simulator.problem
        self.fleet = self.problem.vehicle_fleet
        self.boundaries = (0.0, 1.0)

        self.selection = KTournamentSelection(5)
        self.crossover = SBXCrossover(self.boundaries)
        self.mutation = AddRandomMutation(0.05, self.boundaries)

        self.population_size = 100
        self.chromosome_size = len(self.simulator.problem.customers) - 1    # all customers - depot
        self.iterations = 100

    def solve(self) -> VrpSolution:
        population: Population = Population.create_init_population(
            self.population_size,
            lambda: Chromosome.create_float_chromosome(self.chromosome_size, self.boundaries))
        self.simulator.reset()
        for i in range(self.iterations):
            # evolution
            if i > 0:
                population = population.evolve(self.population_size,
                                               self.selection, self.crossover, self.mutation, elitist=1)
            # simulating all vrp scenarios in population
            for individual in population.individuals:
                decoded_individual = FloatingPointAlgorithm.decode_fpe(individual, self.fleet.number)
                decoded_individual = remove_empty(decoded_individual)
                fitness = self.simulator.simulate(decoded_individual)
                individual.fitness = fitness
        return self.simulator.best_solution

    @staticmethod
    def define_machine(gene: float, m: int) -> int:
        machine = math.modf(gene * m)[1]
        return int(machine)

    @staticmethod
    def decode_fpe(chromosome: Chromosome, m: int) -> List[List[int]]:
        solution = [[] for i in range(m)]
        for customer, gene in enumerate(chromosome.genes, 1):
            vehicle = FloatingPointAlgorithm.define_machine(gene, m)
            solution[vehicle].append(customer)
        return solution


# values in range [0, m)
class RandomKeyEncoding(AbstractAlgorithm):
    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.problem = simulator.problem
        self.fleet = self.problem.vehicle_fleet
        self.boundaries = (0.0, float(self.fleet.number))

        self.selection = KTournamentSelection(5)
        self.crossover = BLXAlphaCrossover(self.boundaries)
        self.mutation = RandomMutation(0.05, self.boundaries)

        self.population_size = 100
        self.chromosome_size = len(self.simulator.problem.customers) - 1
        self.iterations = 100

    def solve(self) -> VrpSolution:
        population: Population = Population.create_init_population(
            self.population_size,
            lambda: Chromosome.create_float_chromosome(self.chromosome_size, self.boundaries))
        self.simulator.reset()
        for i in range(self.iterations):
            # evolution
            if i > 0:
                population = population.evolve(self.population_size,
                                               self.selection, self.crossover, self.mutation, elitist=1)
            # simulating all vrp scenarios in population
            for individual in population.individuals:
                decoded_individual = RandomKeyEncoding.decode_rke(individual, self.fleet.number)
                decoded_individual = remove_empty(decoded_individual)
                fitness = self.simulator.simulate(decoded_individual)
                individual.fitness = fitness
        return self.simulator.best_solution

    @staticmethod
    def decode_rke(chromosome: Chromosome, m: int):
        solution = [[] for _ in range(m)]
        Record = namedtuple('Record', 'machine customer value')
        for customer, gene in enumerate(chromosome.genes, 1):
            fractional, integer = math.modf(gene)
            record = Record(int(integer), customer, fractional)
            try:
                solution[record.machine].append(record)
            except IndexError:
                print(gene)
        resolved_solution = [list(map(lambda r: r.customer, sorted(m, key=lambda r: r.value))) for m in solution]
        return resolved_solution
