from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.ga_utilities import selections, mutations, crossovers
from algorithms.ga_utilities.structs import Population
from data.data_struct import VrpSolution
from simulator.vrp_simulator import VrpSimulator
from typing import List
from algorithms.ga_utilities.structs import Chromosome

class GeneticAlgorithmPermutation(AbstractAlgorithm):

    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.selection = selections.KTournamentSelection(3)
        self.crossover = crossovers.PointCrossover(len(self.simulator.problem.customers)-1)
        self.mutation = mutations.CombinationMuation(0.01, self.simulator.problem.vehicle_fleet.number,  len(self.simulator.problem.customers) )

        self.population_size = 50
        self.chromosome_size = self.simulator.problem.vehicle_fleet.number + len(self.simulator.problem.customers)

        self.iterations = 500

    def solve(self) -> VrpSolution:
        population: Population = Population.create_init_population(
            self.population_size,
            lambda: Chromosome.init_int_chromosome(len(self.simulator.problem.customers), (1, self.chromosome_size)))

        self.simulator.reset()
        for i in range(self.iterations):
            if i > 0:
                population = population.evolve(self.population_size, self.selection, self.crossover, self.mutation)
            for individual in population.individuals:
                fitness = self.simulator.simulate(self.chromosome_to_solution(individual))
                individual.fitness = fitness
        return self.simulator.best_solution

    def chromosome_to_solution(self, individual) -> List[List[int]]:
        routes = []
        br = len(self.simulator.problem.customers)-1
        num_cust =0
        for i in range(br, len(individual.genes)):
            route = []
            for j in range(0, individual.genes[i]):
                route.append(individual.genes[num_cust])
                num_cust = num_cust + 1
            routes.append(route)

        return routes
