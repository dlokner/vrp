from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.ga_utilities import selections, mutations, crossovers
from algorithms.ga_utilities.structs import Population
from data.data_struct import VrpSolution, Route
from simulator.vrp_simulator import VrpSimulator
from typing import List
import collections
from algorithms.ga_utilities.structs import Chromosome
class GeneticAlgorithmMatrix(AbstractAlgorithm):

    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.selection = selections.KTournamentSelection(3)
        self.crossover = crossovers.UniformCrossover(len(self.simulator.problem.customers)-1, self.simulator.problem.vehicle_fleet.number)
        self.mutation = mutations.CombinationMuation(0.01,  len(self.simulator.problem.customers)-1, self.simulator.problem.vehicle_fleet.number)
        self.population_size = 50
        self.chromosome_size = self.simulator.problem.vehicle_fleet.number + len(self.simulator.problem.customers)

        self.iterations = 1000

    def solve(self) -> VrpSolution:
        population: Population = Population.create_init_population(
            self.population_size,
            lambda: Chromosome.init_matrix_chromosome(self.simulator.problem.vehicle_fleet.number, len(self.simulator.problem.customers)-1))
        self.simulator.reset()

        for i in range(self.iterations):
            if i > 0:
                population = population.evolve(self.population_size, self.selection, self.crossover, self.mutation)

            for individual in population.individuals:
                fitness = self.simulator.simulate(self.chromosome_to_solution(individual))
                individual.fitness = fitness
        return self.simulator.best_solution

    def chromosome_to_solution(self, individual) -> List[List[int]]:
        routes = []
        for i in range(0, self.simulator.problem.vehicle_fleet.number):
            d = {}
            route = []
            for j in range(0, len(self.simulator.problem.customers)-1):
                x = individual.genes[i][j]
                if x!=0:
                    d[j+1] = x
            od = collections.OrderedDict(sorted(d.items()))
            for key, value in od.items():
                route.append(key)
            routes.append(route)
        return routes

