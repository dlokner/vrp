from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.ga_utilities.structs import Chromosome
from algorithms.ga_utilities import selections, mutations, crossovers
from algorithms.ga_utilities.structs import Population
from data.data_struct import VrpSolution
from simulator.vrp_simulator import VrpSimulator
from typing import List


class GeneticAlgorithmTest(AbstractAlgorithm):

    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.selection = selections.KTournamentSelection(3)
        self.crossover = crossovers.SinglePointCrossover()
        self.mutation = mutations.SwapGenesMutation(0.05)

        self.population_size = 500
        self.chromosome_size = self.simulator.problem.vehicle_fleet.number + len(self.simulator.problem.customers)

        self.iterations = 10

    def solve(self) -> VrpSolution:
        population: Population = Population.create_init_population(
            self.population_size,
            lambda: Chromosome.create_int_chromosome((0, self.chromosome_size)))
        self.simulator.reset()
        for i in range(self.iterations):
            if i > 0:
                population = population.evolve(self.population_size, self.selection, self.crossover, self.mutation)
            for individual in population.individuals:
                fitness = self.simulator.simulate(self.chromosome_to_solution(individual))
                individual.fitness = fitness
        return self.simulator.best_solution

    def chromosome_to_solution(self, individual) -> List[List[int]]:
        pass
