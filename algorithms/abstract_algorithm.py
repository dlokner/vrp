from typing import List
from simulator.vrp_simulator import VrpSimulator, VrpSolution
from abc import ABC, abstractmethod


class AbstractAlgorithm(ABC):

    def __init__(self, simulator: VrpSimulator):
        self.simulator = simulator

    @abstractmethod
    def solve(self) -> VrpSolution:
        pass
