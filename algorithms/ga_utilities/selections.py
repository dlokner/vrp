from algorithms.ga_utilities.structs import Population, Chromosome
from abc import ABC, abstractmethod
import numpy as np


class AbstractSelection(ABC):
    @abstractmethod
    def select(self, population: Population, size: int):
        pass


class RouletteWheelSelection(AbstractSelection):
    def __init__(self, fitness_sum: float):
        self.fitness_sum = fitness_sum

    def select(self, population: Population, size: int):
        selected = []
        fitness_sum = sum([c.fitness for c in population.individuals])
        for i in range(size):
            selected.append(RouletteWheelSelection.roulette_wheel(population, fitness_sum))
        return selected

    @staticmethod
    def roulette_wheel(population: Population, fitness_sum: float) -> Chromosome:
        pick = np.random.uniform(0.0, fitness_sum)
        current = 0.0
        for chromosome in population.individuals:
            current += chromosome.fitness
            if current > pick:
                return chromosome


class KTournamentSelection(AbstractSelection):
    def __init__(self, k: int):
        KTournamentSelection.check_k_value(k)
        self.k = k

    def select(self, population: Population, size: int):
        if size < 0:
            raise ValueError('Selection sample size must be positive number')
        selection = []
        for _ in range(size):
            selection.append(self.tournament(population))
        return selection

    def tournament(self, population: Population):
        self.check_k_value(self.k)
        indexes = np.random.randint(0, len(population), self.k)
        best = population.get_individual(indexes[0])
        for index in indexes[1:]:
            current = population.get_individual(index)
            best = current if current.fitness < best.fitness else best
        return best

    @staticmethod
    def check_k_value(k):
        if k < 1:
            raise ValueError('K must be number greater or equal to one')
