from algorithms.ga_utilities.structs import Population, Chromosome
from abc import ABC, abstractmethod
import numpy as np
from typing import Tuple


class AbstractMutation(ABC):
    @abstractmethod
    def __init__(self, mutation_rate: float):
        self.rate = mutation_rate

    @abstractmethod
    def mutate(self, population: Population):
        pass


class CombinationMuation(AbstractMutation):
    def __init__(self, mutation_rate: float, num_customers: int, vehicle_num: int):
        self.mutations: List[AbstractMutation] = [InverseMutation, ToggleMutation]
        self.num_customers = num_customers
        self.vehicle_num = vehicle_num
        self.rate = mutation_rate


    def mutate(self, population):
        mutation = np.random.choice(self.mutations)(self.rate, self.num_customers, self.vehicle_num)
        for individual in population.individuals:
            mutation.mutate_chromosome(individual)


# =====================================
#       Real-coded GA Mutations
# =====================================

class SwapGenesMutation(AbstractMutation):
    def __init__(self, mutation_rate: float):
        super().__init__(mutation_rate)

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome(individual)

    def mutate_chromosome(self, c: Chromosome):
        for i in range(len(c)):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                swap_index = np.random.randint(0, len(c))
                c.genes[i], c.genes[swap_index] = c.genes[swap_index], c.genes[i]


class RandomMutation(AbstractMutation):
    def __init__(self, mutation_rate, boundaries: Tuple[float, float]):
        super().__init__(mutation_rate)
        self.boundaries = boundaries

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome(individual)

    def mutate_chromosome(self, c: Chromosome):
        for i in range(len(c)):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                c.genes[i] = np.random.uniform(self.boundaries[0], self.boundaries[1])


class AddRandomMutation(AbstractMutation):
    def __init__(self, mutation_rate, boundaries: Tuple[float, float], c=0.25):
        super().__init__(mutation_rate)
        self.boundaries = boundaries
        self.c = c

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome(individual)

    def mutate_chromosome(self, c: Chromosome):
        for i in range(len(c)):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                s = c.get_gene(i) + np.random.uniform(self.boundaries[1], self.boundaries[1]) * self.c
                s = self.keep_in_bounds(s)
                c.genes[i] = s

    def keep_in_bounds(self, s: float):
        if s < self.boundaries[0]:
            return self.boundaries[0]
        elif s >= self.boundaries[1]:
            return self.boundaries[1] - 1e-10
        return s

# =======================================
#       Matrix GA Crossovers
# =======================================

class CellValue(AbstractMutation):
    def __init__(self, mutation_rate: float, num_customers: int, vehicle_num: int):
        super().__init__(mutation_rate)
        self.num_customers = num_customers
        self.vehicle_num = vehicle_num

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome_matrix(individual, self.num_customers, self.vehicle_num)

    def mutate_chromosome_matrix(self, c, num_customers, vehicle_num):  # cell swap
        for i in range(len(c)):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                for j in range(0, vehicle_num):
                    if c.genes[j, i] != 0:
                        x = round(np.random.uniform(0.01, 0.99), 3)
                        c.genes[j, i] = x


class CellSwap(AbstractMutation):
    def __init__(self, mutation_rate: float, num_customers: int, vehicle_num: int):
        super().__init__( mutation_rate)
        self.num_customers = num_customers
        self.vehicle_num = vehicle_num

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome_matrix(individual, self.num_customers, self.vehicle_num)

    def mutate_chromosome_matrix(self, c, num_customers, vehicle_number):  # cell swap
        for i in range(num_customers):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                for j in range(0, vehicle_number):
                    if c.genes[j, i] != 0.0:
                        m = np.random.randint(0, vehicle_number - 1)
                        if(m!=j):
                            c.genes[m, i] = c.genes[j, i]
                            c.genes[j, i] = 0.0




# =======================================
#       Permutation GA Crossovers
# =======================================

class InverseMutation(AbstractMutation):
    def __init__(self, mutation_rate: float, num_customers: int, vehicle_num: int):
        super().__init__(mutation_rate)
        self.num_customers = num_customers
        self.vehicle_num = vehicle_num

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome(individual)

    def mutate_chromosome(self, c):
        for i in range(len(c.genes) - self.num_customers):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                swap_index_one_first = np.random.randint(0, self.num_customers)
                swap_index_one_second = np.random.randint(swap_index_one_first, self.num_customers)
                swap_index_two_first = np.random.randint(0, c.size() - self.num_customers)
                swap_index_two_second = np.random.randint(swap_index_two_first, c.size() - self.num_customers)
                k = 0

                for i in range(swap_index_one_first, int((swap_index_one_second+swap_index_one_first)/2)):
                    el = c.genes[swap_index_one_second-k]
                    c.genes[swap_index_one_second-k] = c.genes[i]
                    c.genes[i] = el
                    k += 1
                k = 0
                for i in range(swap_index_two_first, int((swap_index_two_second+swap_index_two_first)/2)):
                    el = c.genes[swap_index_two_second - k]
                    c.genes[swap_index_two_second - k] = c.genes[i]
                    c.genes[i] = el
                    k += 1


class ToggleMutation(AbstractMutation):
    def __init__(self, mutation_rate: float, num_customers: int, vehicle_num: int):
        super().__init__(mutation_rate)
        self.num_customers = num_customers
        self.vehicle_num = vehicle_num

    def mutate(self, population: Population):
        for individual in population.individuals:
            self.mutate_chromosome(individual)

    def mutate_chromosome(self, c):
        self.num_customers = self.num_customers
        for i in range(len(c.genes), self.num_customers):
            if np.random.choice([True, False], p=[self.rate, 1 - self.rate]):
                swap_index_one = np.random.randint(0, self.num_customers)
                swap_index_two = np.random.randint(0, c.size() - self.num_customers)
                list_one = c.genes[:self.num_customers]
                list_two = c.genes[self.num_customers:]
                list_one[i], list_one[swap_index_one] = list_one[swap_index_one], list_one[i]
                list_two[i], list_two[swap_index_two] = list_two[swap_index_two], list_two[i]
                c.genes[:self.num_customers] = list_one
                c.genes[self.num_customers:] = list_two

