from algorithms.ga_utilities.structs import Chromosome
from abc import ABC, abstractmethod
import numpy as np
import math
from typing import List


def check_sizes(c1, c2):
    if len(c1) != len(c2):
        raise ValueError('Chromosomes sizes do not match')


def check_alpha(alpha):
    if alpha < 0.0 or alpha > 1.0:
        raise ValueError('Alpha must be float value in range [0.0, 1.0]')


def arithmetic_crossover(c1, c2, alpha: float):
    check_alpha(alpha)
    offspring = c1 * alpha + c2 * (1 - alpha)
    return offspring


class AbstractCrossover(ABC):
    @abstractmethod
    def cross(self, c1: Chromosome, c2: Chromosome):
        pass


class CombinationCrossover(AbstractCrossover):
    def __init__(self, crossovers: List[AbstractCrossover]):
        self.crossovers: List[AbstractCrossover] = crossovers

    def cross(self, c1: Chromosome, c2: Chromosome):
        crossover = np.random.choice(self.crossovers)
        offspring = crossover.cross(c1, c2)
        return offspring


# =======================================
#       Real-Coded GA Operators
# =======================================

class DiscreteCrossover(AbstractCrossover):

    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        offspring_arr = []
        for i in range(c1.size()):
            if np.random.choice([True, False], p=[0.5, 0.5]):
                offspring_arr.append(c1.get_gene(i))
            else:
                offspring_arr.append(c2.get_gene(i))
        offspring = Chromosome(np.array(np.asarray(offspring_arr)))
        return offspring


class SimpleArithmeticCrossover(AbstractCrossover):
    def __init__(self, alpha: float = 0.5):
        check_alpha(alpha)
        self.alpha = alpha

    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        k = np.random.randint(0, c1.size())
        chosen = c1 if np.random.choice([True, False], p=[0.5, 0.5]) else c2
        random_parent_part = chosen.genes[:k]
        arithmetic_part = arithmetic_crossover(c2.genes, c1.genes, self.alpha)[k:]
        offspring = np.concatenate([random_parent_part, arithmetic_part])
        return Chromosome(offspring)


class WholeArithmeticCrossover(AbstractCrossover):
    def __init__(self, alpha: float = 0.5):
        check_alpha(alpha)
        self.alpha = alpha

    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        offspring_arr = arithmetic_crossover(c1.genes, c2.genes, self.alpha)
        return Chromosome(offspring_arr)


class LocalCrossover(AbstractCrossover):
    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        alpha = np.random.uniform(0, 1, len(c1))
        offspring = c1.genes * alpha + c2.genes * (1 - alpha)
        return Chromosome(offspring)


class FlatCrossover(AbstractCrossover):
    def cross(self, c1: Chromosome, c2: Chromosome):
        return Chromosome(np.random.uniform(c1.genes, c2.genes))


class SBXCrossover(AbstractCrossover):
    def __init__(self, boundaries, q=3):
        self.q = q
        self.boundaries = boundaries

    def cross(self, c1: Chromosome, c2: Chromosome):
        b = self.calc_coefficient(float(np.random.rand()))
        offspring1 = 1 / 2 * (c1.genes * (1 + b) + c2.genes * (1 - b))
        offspring2 = 1 / 2 * (c1.genes * (1 - b) + c2.genes * (1 + b))
        offspring = offspring1 if np.random.choice([True, False]) else offspring2
        offspring = Chromosome(offspring)
        offspring.keep_in_bounds(self.boundaries)
        return offspring

    def calc_coefficient(self, u: float):
        exp = 1 / (1 + self.q)
        if u <= 0.5:
            return (2 * u) ** exp
        else:
            return (1 / (2 * (1 - u))) ** exp

    @staticmethod
    def check_u(u: float):
        if u < 0.0 or u > 1.0:
            raise ValueError('u coefficient must be number in range [0.0, 1.0]')


class BLXAlphaCrossover(AbstractCrossover):
    def __init__(self, boundaries, a=0.5):
        self.a = a
        self.boundaries = boundaries

    def cross(self, c1: Chromosome, c2: Chromosome):
        max_arr = np.maximum(c1.genes, c2.genes)
        min_arr = np.minimum(c1.genes, c2.genes)
        i_arr = max_arr - min_arr
        alpha = np.random.rand(c1.size())

        mul_i_a = self.a * i_arr
        res_min = min_arr - mul_i_a
        res_max = max_arr + mul_i_a
        offspring = res_min + alpha * np.abs(res_max - res_min)
        offspring = Chromosome(offspring)
        offspring.keep_in_bounds(self.boundaries)
        return offspring


# ======================================
#   Permutation-based GA Operators
# ======================================

class SinglePointCrossover(AbstractCrossover):

    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        chromosome_size = c1.size()
        point = np.random.randint(0, chromosome_size)
        return Chromosome(np.concatenate((c1.genes[:point], c2.genes[point:])))


# =======================================
#       Matrix-encoded GA Crossover
# =======================================

class UniformCrossover(AbstractCrossover):
    def __init__(self, num_customers: int, num_vehicles):
        self.num_customers = num_customers
        self.num_vehicles = num_vehicles

    def cross(self, c1: Chromosome, c2: Chromosome):
        child1 = np.empty(shape=(self.num_vehicles, self.num_customers))
        child2 = np.empty(shape=(self.num_vehicles, self.num_customers))

        for i in range(0, self.num_customers):
            n = np.random.randint(0, 1)
            if n == 0:
                child1[:, i] = c1.genes[:, i]
                child2[:, i] = c2.genes[:, i]
            else:
                child1[:, i] = c2.genes[:, i]
                child2[:, i] = c2.genes[:, i]
        return Chromosome(child1)


class PointCrossover(AbstractCrossover):

    def __init__(self, num_customers: int):
        self.num_customers = num_customers

    def cross(self, c1: Chromosome, c2: Chromosome):
        check_sizes(c1, c2)
        parent1 = c1.genes[:self.num_customers]
        parent2 = c2.genes[:self.num_customers]
        vehicle_one = c1.genes[self.num_customers:]
        vehicle_two = c2.genes[self.num_customers:]
        point_customar = np.random.randint(0, self.num_customers)
        list_one_cust = []
        list_two_cust = []
        for i in range(0, point_customar):
            list_one_cust.append(parent1[i])
        for x in parent2:
            if x not in list_one_cust:
                list_one_cust.append(x)

        return Chromosome(np.concatenate([list_one_cust, vehicle_one]))