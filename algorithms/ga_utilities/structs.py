import numpy as np
from typing import List, Tuple, Union, Callable
from operator import attrgetter


class Chromosome:
    def __init__(self, chromosome: np.ndarray = None, fitness=0.0):
        self.genes = chromosome
        self.fitness = fitness

    def __len__(self):
        return self.size()

    @staticmethod
    def init_int_chromosome(num_customers: int, bounds: tuple):
        n = bounds[1] - num_customers
        chromosome = Chromosome()
        first_part = np.arange(bounds[0], num_customers)
        second_part = np.random.multinomial(num_customers-1, np.ones(n) / n, size=1)[0]
        np.random.shuffle(first_part)
        np.random.shuffle(second_part)
        chromosome.genes = np.concatenate([first_part, second_part])
        return chromosome

    @staticmethod
    def init_matrix_chromosome(vehicle_number, customer_num):
        chromosome = Chromosome()
        min = 0.01
        max = 0.99
        matrix = np.zeros(shape=(vehicle_number, customer_num))
        numbers = np.random.uniform(min, max, customer_num)
        for i in range(0, customer_num):
            n = np.random.randint(0, vehicle_number - 1)
            matrix[n][i] = round(numbers[i],5)
        chromosome.genes = matrix
        return chromosome

    @staticmethod
    def create_float_chromosome(length: int, bounds: tuple):
        chromosome = Chromosome()
        chromosome.genes = np.random.uniform(bounds[0], bounds[1], length)
        return chromosome

    def get_gene(self, index: int) -> int:
        return self.genes[index]

    def size(self):
        return 0 if self.genes is None else len(self.genes)

    def copy(self):
        chromosome_copy = self.genes.copy()
        return Chromosome(chromosome_copy)

    def keep_in_bounds(self, boundaries: Union[Tuple[int, int], Tuple[float, float]], type_float=True):
        def func(gene):
            if gene >= boundaries[1]:
                return boundaries[1] - (1e-10 if type_float else 1)
            elif gene < boundaries[0]:
                return boundaries[0]
            else:
                return gene
        self.genes = np.array(list(map(func, self.genes)))

    def __str__(self):
        return np.array2string(self.genes)


class Population:
    def __init__(self, population: List[Chromosome] = None):
        if population is None:
            population = []
        self.individuals = population

    def __len__(self):
        return self.size()

    def copy(self):
        p = Population(self.individuals.copy())
        return p

    def size(self):
        return len(self.individuals)

    def add(self, chromosome: Chromosome):
        self.individuals.append(chromosome)

    def extend(self, chromosomes: List[Chromosome]):
        self.individuals.extend(chromosomes)

    def remove(self, chromosome: Chromosome):
        self.individuals.remove(chromosome)

    def elitist_select(self, size: int) -> List[Chromosome]:
        return sorted(self.individuals, key=lambda i: i.fitness, reverse=True)[:size]

    def get_individual(self, index: int) -> Chromosome:
        return self.individuals[index]

    def evolve(self, size: int, selection, crossover, mutation, elitist=0):
        self.check_population()
        parents = selection.select(self, (size - elitist) * 2)
        children = self.cross_parents(parents, crossover)
        new_population = Population(children)
        mutation.mutate(new_population)
        elite = self.elitist_select(elitist)
        new_population.extend(elite)
        return new_population

    @staticmethod
    def create_init_population(population_size: int, chromosome_factory: Callable[[], Chromosome]):
        individuals: List[Chromosome] = []
        for i in range(population_size):
            chromosome: Chromosome = chromosome_factory()
            individuals.append(chromosome)
        return Population(individuals)

    @staticmethod
    def cross_parents(parents: List[Chromosome], crossover_function):
        if len(parents) % 2 != 0:
            raise ValueError('Odd number of parents.')
        children: List[Chromosome] = []
        for i in range(0, len(parents), 2):
            child = crossover_function.cross(parents[i], parents[i + 1])
            children.append(child)
        return children

    def sort_population_by_fitness(self, reverse=False) -> List[Chromosome]:
        self.individuals.sort(key=lambda c: c.fitness, reverse=reverse)
        return self.individuals

    def highest_fitness(self):
        if self.individuals is None:
            return None
        else:
            return max(self.individuals, key=attrgetter('fitness'))

    def check_population(self):
        if self.individuals is None:
            raise ValueError('Population is not defined!')
