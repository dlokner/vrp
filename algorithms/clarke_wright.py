from typing import List, Callable
from data.data_struct import VrpSolution, VehicleFleet, Customer, Route
from simulator.vrp_simulator import VrpSimulator
from algorithms.abstract_algorithm import AbstractAlgorithm
from collections import namedtuple

Saving = namedtuple("Saving", "c1 c2 value")


class ClarkWrightAlgorithm(AbstractAlgorithm):
    def __init__(self, simulator: VrpSimulator):
        super().__init__(simulator)
        self.depot: Customer = self.simulator.depot
        self.customers = self.simulator.problem.customers[1:]
        self.vehicle_fleet: VehicleFleet = self.simulator.problem.vehicle_fleet

    def solve(self):
        savings: List[Saving] = self.calculate_all_savings()
        ClarkWrightAlgorithm.sort_savings(savings, descending=True)
        return self.create_routes(savings)

    def create_routes(self, savings: List[Saving]):
        savings_copy = savings.copy()
        routes: List[List[Customer]] = []

        def merge_test(r1: List[Customer], r2: List[Customer], saving: Saving):
            return ClarkWrightAlgorithm.test(saving, self.vehicle_fleet.capacity, r1, r2)

        # include link (i, j) in a route if no route constraints
        # will be violated through the inclusion of (i, j) in a route, and if
        for s in savings:
            visited_customers = ClarkWrightAlgorithm.routes_contains_link_customers(routes, s)
            # Or, exactly one of the two points (i or j) has already been included in an existing route
            # and that point is not interior to that route,
            # in which case the link (i, j) is added to that same route.
            if visited_customers == 1:
                potential_route = ClarkWrightAlgorithm.can_append_link(routes, s)
                if potential_route and ClarkWrightAlgorithm.test(s, self.vehicle_fleet.capacity, potential_route):
                    ClarkWrightAlgorithm.append_link(potential_route, s, hard=True)
                    savings_copy.remove(s)
                    continue
            # Or, both i and j have already been included in two different existing routes
            # and neither point is interior to its route, in which case the two routes are merged.
            elif visited_customers == 2:
                if ClarkWrightAlgorithm.merge_if_true(routes, saving=s, test=merge_test):
                    savings_copy.remove(s)
            # Either, neither i nor j have already been assigned to a route,
            # in which case a new route is initiated including both
            else:
                if ClarkWrightAlgorithm.test(s, self.vehicle_fleet.capacity):
                    routes.append(ClarkWrightAlgorithm.create_route(s))
                    savings_copy.remove(s)

        if not savings_copy:
            routes.extend(ClarkWrightAlgorithm.create_routes_of_unused_savings(savings_copy))

        solution = self.create_vrp_solution(routes)
        return solution

    def calculate_all_savings(self) -> List[Saving]:
        savings: List[Saving] = []
        for i, c1 in enumerate(self.customers[:-1]):
            for j in range(i+1, len(self.customers)):
                c2 = self.customers[j]
                saving = self.define_saving(c1, c2)
                savings.append(saving)
        return savings

    def define_saving(self, c1: Customer, c2: Customer) -> Saving:
        value = self.calc_saving(c1, c2)
        saving = Saving(c1, c2, value)
        return saving

    def calc_saving(self, c1: Customer, c2: Customer) -> float:
        d_to_c1 = self.simulator.distance(self.depot, c1)
        d_to_c2 = self.simulator.distance(self.depot, c2)
        c1_to_c2 = self.simulator.distance(c1, c2)
        return d_to_c1 + d_to_c2 - c1_to_c2

    def create_vrp_solution(self, customer_routes: List[List[Customer]]) -> VrpSolution:
        routes: List[Route] = []
        for i, r in enumerate(customer_routes):
            routes.append(Route(r, name=str(i)))
        solution = VrpSolution(routes)
        solution.fitness = self.simulator.simulate(solution.simplify())
        return solution

    @staticmethod
    def create_route(link: Saving) -> List[Customer]:
        return [link.c1, link.c2]

    @staticmethod
    def sort_savings(savings: List[Saving], descending=False) -> None:
        savings.sort(key=lambda s: s.value, reverse=descending)

    # CONTAINS
    @staticmethod
    def routes_contains_link_customers(routes: List[List[Customer]], link: Saving) -> int:
        counter = 0
        for route in routes:
            counter += 1 if ClarkWrightAlgorithm.route_contains_customer(route, link.c1) else 0
            counter += 1 if ClarkWrightAlgorithm.route_contains_customer(route, link.c2) else 0
        return counter

    @staticmethod
    def routes_contains_any(routes: List[List[Customer]], link: Saving) -> bool:
        return any(r for r in routes if ClarkWrightAlgorithm.route_contains_any(r, link))

    @staticmethod
    def routes_contains_all(routes: List[List[Customer]], link: Saving) -> bool:
        counter = 0
        for r in routes:
            if ClarkWrightAlgorithm.route_contains_any(r, link):
                counter += 1
        return counter > 1

    @staticmethod
    def route_contains_any(route: List[Customer], link: Saving) -> bool:
        return any(c for c in route if link.c1 == c or link.c2 == c)

    @staticmethod
    def route_contains_all(route: List[Customer], link: Saving) -> bool:
        return link.c1 in route and link.c2 in route

    @staticmethod
    def route_contains_customer(route: List[Customer], customer: Customer):
        return customer in route

    # DEMAND

    @staticmethod
    def calc_saving_demand(saving: Saving) -> int:
        demand = saving.c1.demand + saving.c2.demand
        return demand

    @staticmethod
    def calc_route_demand(route: List[Customer]):
        demand = 0
        for c in route:
            demand += c.demand
        return demand

    @staticmethod
    def is_exterior(route: List[Customer], c: Customer) -> bool:
        first, last = route[0], route[-1]
        return c == first or c == last

    # APPEND LINK

    @staticmethod
    def can_append_link(routes: List[List[Customer]], link: Saving) -> List[Customer] or None:
        for route in routes:
            condition1 = ClarkWrightAlgorithm.is_exterior(route, link.c1)
            condition2 = ClarkWrightAlgorithm.is_exterior(route, link.c2)
            if (condition1 or condition2) and not (condition1 and condition2):
                if not ClarkWrightAlgorithm.route_contains_all(route, link):
                    return route
        return None

    @staticmethod
    def append_link(route: List[Customer], saving: Saving, hard=False) -> List[Customer] or None:
        if not hard:
            route = route.copy()
        first, last = route[0], route[-1]
        c1, c2 = saving.c1, saving.c2
        if first in saving:
            route.insert(0, c1) if first == c2 else route.insert(0, c2)
            return route
        elif last in saving:
            route.append(c1) if last == c2 else route.append(c2)
            return route
        return None

    # MERGE ROUTES

    @staticmethod
    def merge_routes_by_link(r1: List[Customer], r2: List[Customer], link: Saving):
        f1, f2, l1, l2 = r1[0], r2[1], r1[0], r2[1]
        c1, c2 = link.c1, link.c2
        if (f1 == c1 or l1 == c1) and (f2 == c2 and l2 == c2) or (f1 == c2 or l1 == c2) and (f2 == c1 and l2 == c1):
            return ClarkWrightAlgorithm.merge_routes(r1, r2, link)

    @staticmethod
    def merge_routes(r1: List[Customer], r2: List[Customer], link: Saving) -> List[Customer] or None:
        r1 = ClarkWrightAlgorithm.append_link(r1, link, hard=False)
        f1, l1 = r1[0], r1[-1]
        f2, l2 = r2[0], r2[-1]
        if l1 == f2:
            return r1 + r2[1:]
        elif f1 == l2:
            return r2 + r1[1:]
        elif f1 == f2:
            return list(reversed(r1)) + r2[1:]
        elif l1 == l2:
            return r1 + list(reversed(r2))[1:]
        else:
            return None

    @staticmethod
    def merge_if_true(routes: List[List[Customer]],
                      saving: Saving,
                      test: Callable[[List[Customer], List[Customer], Saving], bool],
                      keep_position=True):
        for i, r1 in enumerate(routes[:-1]):
            for j in range(i+1, len(routes)):
                r2 = routes[j]
                if test(r1, r2, saving):
                    res = ClarkWrightAlgorithm.merge_routes_by_link(r1, r2, saving)
                    if res:
                        routes.remove(r1)
                        routes.remove(r2)
                        if keep_position:
                            routes.insert(i, res)
                        else:
                            routes.append(res)
                        return

    @staticmethod
    def create_routes_of_unused_savings(savings: List[Saving]) -> List[List[Customer]]:
        customers: List[List[Customer]] = []
        for s in savings:
            customers.append([s.c1])
            customers.append([s.c2])
        return customers

    @staticmethod
    def test(saving: Saving, bound: int, *args) -> bool:
        d = 0
        for route in args:
            d += ClarkWrightAlgorithm.calc_route_demand(route)
        if saving:
            d += ClarkWrightAlgorithm.calc_saving_demand(saving)
        return d <= bound
