from data.vrpIO import *
from simulator.vrp_simulator import VrpSimulator
from algorithms.genetic_algorithms.genetic_algorithm_permutation import GeneticAlgorithmPermutation


instance = read_vrp_instance('files/C101.txt')
simulator = VrpSimulator(instance)
algorithm = GeneticAlgorithmPermutation(simulator)
solution = algorithm.solve()

list_up = []
list_down = []
for route in solution.routes:
    if len(route.customers)>0:
        list_down.append(len(route.customers))
        for cust in route.customers:
            list_up.append(cust.number)
print(len(list_up))
print(list_up)
print(list_down)
write_vrp_solution('solution_permutation.txt', 'w', solution)




