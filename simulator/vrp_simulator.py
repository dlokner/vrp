from data.data_struct import *
from typing import List, Tuple, Dict
import math


class VrpSimulator:
    def __init__(self, problem: SolomonsInstance, time_window=False):
        self.problem = problem
        self.customer_mapper = CustomersMapper(problem.customers)
        self.depot: Customer = self.extract_depot()

        self.time_window = time_window

        self.best_solution: VrpSolution or None = None

        self.distances: Dict[Tuple[Tuple[int, int], Tuple[int, int]], float] = {}

    def map_solution(self, solution: List[List[int]]) -> VrpSolution:
        routes: List[Route] = []
        for index, s in enumerate(solution):
            route = Route(self.customer_mapper.map_customers(s), name=str(index))
            routes.append(route)
        return VrpSolution(routes)

    def simulate(self, solution: List[List[int]]) -> float:
        mapped_solution = self.map_solution(solution)
        fitness = 0.0
        for route in mapped_solution.routes:
            capacity, overcapacity, distance, overtime, late = self.route_cost(route)
            fitness += self.calculate_fitness(capacity, overcapacity, distance, overtime, late)
        mapped_solution.fitness = fitness
        self.best_solution = mapped_solution if \
            not self.best_solution or self.best_solution.fitness > mapped_solution.fitness else self.best_solution
        return fitness

    def route_cost(self, route: Route) -> Tuple[float, float, float, float, int]:
        total_capacity = route.calc_route_capacity()
        overcapacity = self.calc_overcapacity(total_capacity)
        total_distance = 0.0
        total_time = self.depot.s_time
        total_overtime = 0.0
        late_counter = 0

        last = self.depot
        customers = route.customers
        customers.append(self.depot)
        for c in customers:
            distance = self.distance(last, c)
            total_distance += distance
            total_time += distance

            overtime, service_time, late = VrpSimulator.service_cost(total_time, c)
            total_time += 0 if late else overtime + service_time
            total_overtime += overtime
            late_counter += 1 if late else 0

            last = c
        customers.remove(self.depot)
        return total_capacity, overcapacity, total_distance, total_overtime, late_counter

    def reset(self):
        self.best_solution = None

    def use_time_window(self, flag=True):
        self.time_window = flag

    @staticmethod
    def service_cost(current_time: float, customer: Customer) -> Tuple[float, float, bool]:
        overtime = customer.s_time - current_time if current_time <= customer.s_time else 0
        late = current_time > customer.e_time
        service_time = customer.service_duration
        return overtime, service_time, late

    def distance(self, start: Customer, destination: Customer) -> float:
        s_point = start.coordinate
        d_point = destination.coordinate
        coordinates = (s_point, d_point)

        cached_distance = self.distances.get(coordinates)

        if cached_distance:
            return cached_distance
        distance = math.sqrt((s_point[0] - d_point[0])**2 + (s_point[1] - d_point[1])**2)
        self.distances.update({coordinates: distance})

        return distance

    def calc_overcapacity(self, capacity: float):
        max_capacity = self.problem.vehicle_fleet.capacity
        return max(capacity - max_capacity, 0.0)

    def calculate_fitness(self, capacity, overcapacity, distance, overtime, late) -> float:
        fitness = distance + 1000 * overcapacity
        if self.time_window:
            fitness += overtime
            if late > 0:
                fitness *= late * self.problem.vehicle_fleet.number
        return fitness

    def extract_depot(self) -> Customer:
        return self.customer_mapper.get_customer_by_number(0)


class CustomersMapper:
    def __init__(self, customers: List[Customer]):
        self.customers = customers
        self.customers_number = [c.number for c in customers]

    def get_customer_by_number(self, customer_number: int):
        index = self.customers_number.index(customer_number)
        if index == -1:
            return None
        return self.customers[index]

    def map_customers(self, customer_numbers: List[int]) -> List[Customer]:
        mapped_customers: List[Customer] = []
        for c_num in customer_numbers:
            customer = self.get_customer_by_number(c_num)
            if customer:
                mapped_customers.append(customer)
            else:
                raise ValueError('Customer with that number is not present in the list')
        return mapped_customers
