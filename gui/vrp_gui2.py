from tkinter import *
from tkinter import filedialog
from tkinter import messagebox
from tkinter.ttk import Progressbar

import random
from typing import Union, Dict, Type

from data.data_struct import *
from data.vrpIO import *
from simulator.vrp_simulator import VrpSimulator

from algorithms.abstract_algorithm import AbstractAlgorithm
from algorithms.clarke_wright import ClarkWrightAlgorithm
from algorithms.genetic_algorithms.real_coded_ga import FloatingPointAlgorithm, RandomKeyEncoding

FILES_FOLDER = './files/'


class StatusBar(Label):
    UNDEFINED = 'Route cost is not defined'
    DEFINED = 'Route cost: '

    def __init__(self, parent, text=UNDEFINED):
        self.text_var = StringVar()
        self.text_var.set(text)
        super().__init__(parent, textvariable=self.text_var)

    def set_text(self, text: str):
        self.text_var.set(text)


class LoadingDialog(Toplevel):
    def __init__(self, message):
        super().__init__()
        self.message = message
        label = Label(self, text=message)
        label.pack(fill=BOTH)
        Progressbar(self, orient=HORIZONTAL, length=100, mode='indeterminate').pack(fill=X, pady=10)
        self.update_idletasks()

    def quit(self):
        self.destroy()


class AlgorithmsCollection:
    def __init__(self):
        self.algorithms: Dict[str, Type[AbstractAlgorithm]] = {
            'clark': ClarkWrightAlgorithm,
            'floating': FloatingPointAlgorithm,
            'random_key': RandomKeyEncoding
        }
        self.descriptions: Dict[str, str] = {
            'clark': 'Clark and Wright Algorithm',
            'floating': 'GA: Floating Point',
            'random_key': 'GA: Random key encoding'
        }

    def get_key_by_desc(self, desc: str):
        for k in self.descriptions.keys():
            if self.descriptions.get(k) == desc:
                return k
        return None

    def get_keys(self):
        return self.algorithms.keys()

    def get_text(self, key: str):
        return self.descriptions.get(key)

    def solve(self, algorithm_key: str, simulator: VrpSimulator) -> VrpSolution:
        algorithm = self.algorithms.get(algorithm_key)(simulator)
        solution = algorithm.solve()
        return solution


class VehicleDialog(Tk):

    def __init__(self):
        super().__init__()
        self.labels: List[Label] = []
        self.entries: List[Entry] = []

        self.labels.append(Label(self, text='Number of vehicles: ').grid(row=0, column=0))
        self.labels.append(Label(self, text='Capacity: ').grid(row=1, column=0))

        self.num = 25
        self.capacity = 900

        self.num_entry = Entry(self)
        self.capacity_entry = Entry(self)

        self.num_entry.insert(0, '25')
        self.capacity_entry.insert(0, '500')

        self.num_entry.grid(row=0, column=1)
        self.capacity_entry.grid(row=1, column=1)

        self.vehicle_fleet = None

        self.widgets = list(zip(self.labels, self.entries))
        self.submit1 = Button(self, text='Submit', command=self.on_submit)
        self.submit1.grid(row=2, column=1, sticky=E, padx=10, pady=10)
        self.wait_window(self)

    def get_vehicle_fleet(self):
        return self.vehicle_fleet

    def on_submit(self):
        self.entries.append(self.num_entry)
        self.entries.append(self.capacity_entry)

        for entry in self.entries:
            if len(entry.get()) == 0:
                print(entry.get)
                messagebox.showerror("Error", "All entries must be filled.")
                self.vehicle_dialog = VehicleDialog()
                self.vehicle_dialog.title('Depot details')
                self.vehicle_dialog.geometry("250x80")
                self.destroy()
                break

        for entry in self.entries:
            if not entry.get().isdigit():
                messagebox.showerror("Error", entry.get() + " must be integer.")
                self.vehicle_dialog = VehicleDialog()
                self.vehicle_dialog.title('Depot details')
                self.vehicle_dialog.geometry("250x80")
                self.destroy()
                break

        self.num = int(self.num_entry.get())
        self.capacity = int(self.capacity_entry.get())
        self.vehicle_fleet = VehicleFleet(self.num, self.capacity)
        self.destroy()


class CustomerDialog(Tk):

    def __init__(self, x, y, num, list_customers):
        super().__init__()
        self.labels: List[Label] = []
        self.entries: List[Entry] = []

        self.x = x
        self.y = y
        self.num = num
        self.list_customers = list_customers

        self.labels.append(Label(self, text='Name: ').grid(row=0, column=0))
        self.labels.append(Label(self, text='Cargo: ').grid(row=1, column=0))
        self.labels.append(Label(self, text='Ready time: ').grid(row=2, column=0))
        self.labels.append(Label(self, text='Due date: ').grid(row=3, column=0))
        self.labels.append(Label(self, text='Service time: ').grid(row=4, column=0))

        self.name = ''
        self.cargo = 0
        self.ready_time = 0
        self.due_date = 0
        self.service_time = 0
        self.entries.clear()

        self.name_entry = Entry(self)
        self.cargo_entry = Entry(self)
        self.ready_time_entry = Entry(self)
        self.due_date_entry = Entry(self)
        self.service_time_entry = Entry(self)

        self.name_entry.insert(0, 'Undefined')
        self.cargo_entry.insert(0, '50')
        self.ready_time_entry.insert(0, '0')
        self.due_date_entry.insert(0, '900')
        self.service_time_entry.insert(0, '2')

        self.name_entry.grid(row=0, column=1)
        self.cargo_entry.grid(row=1, column=1)
        self.ready_time_entry.grid(row=2, column=1)
        self.due_date_entry.grid(row=3, column=1)
        self.service_time_entry.grid(row=4, column=1)

        self.widgets = list(zip(self.labels, self.entries))
        self.submit = Button(self, text='Submit', command=self.on_submit)
        self.submit.grid(row=5, column=1, sticky=E, padx=10, pady=10)

    def on_submit(self):
        self.entries.append(self.name_entry)
        self.entries.append(self.cargo_entry)
        self.entries.append(self.ready_time_entry)
        self.entries.append(self.due_date_entry)
        self.entries.append(self.service_time_entry)

        for entry in self.entries:
            if len(entry.get()) == 0:
                messagebox.showerror("Error", "All entries must be filled.")
                self.customer_dialog = CustomerDialog(self.x, self.y, self.num, self.list_customers)
                self.customer_dialog.title('Customer details')
                self.customer_dialog.geometry("270x145")
                self.customer_dialog.resizable(width=True, height=True)

                self.destroy()
                break

        for i in range(1, len(self.entries)):
            if not self.entries[i].get().isdigit():
                messagebox.showerror("Error", self.entries[i].get() + " must be integer.")
                customer_dialog = CustomerDialog(self.x, self.y, self.num, self.list_customers)
                customer_dialog.title('Customer details')
                customer_dialog.geometry("270x145")
                customer_dialog.resizable(width=True, height=True)
                self.destroy()
                break

        self.name = self.name_entry.get()
        self.cargo = int(self.cargo_entry.get())
        self.ready_time = int(self.ready_time_entry.get())
        self.due_date = int(self.due_date_entry.get())
        self.service_time = int(self.service_time_entry.get())

        self.customer = Customer.create_customer(self.num, self.x, self.y, self.cargo, self.ready_time, self.due_date,
                                                 self.service_time)
        self.list_customers.append(self.customer)
        self.destroy()

    def get_new_customer(self):
        return self.customar

    def get_customers(self):
        return self.list_customers


class CoordinatesCollection:
    def __init__(self):
        self.problem: Union[SolomonsInstance, None] = None
        self.solution: Union[VrpSolution, None] = None

        self.customers: Union[List[Customer], None] = None
        self.depots: Union[List[Customer], None] = None

    def set_problem(self, problem: SolomonsInstance):
        self.problem = problem
        self.customers = self.problem.customers[1:]
        self.depots = [self.problem.customers[0]]

    def get_depot_index(self, depot: Customer):
        return self.depots.index(depot)

    def get_customer_index(self, customer: Customer):
        return self.customers.index(customer)

    @staticmethod
    def extract_coordinates(customers: List[Customer]) -> List[tuple]:
        return [c.coordinate for c in customers]

    @staticmethod
    def get_minmax_x_y(coordinates: List[Tuple[int, int]]) -> Union[Tuple[Tuple[int, int], Tuple[int, int]], None]:
        if not coordinates:
            return None
        x = y = []
        for c in coordinates:
            x.append(c[0])
            y.append(c[1])
        return (min(x), min(y)), (max(x), max(y))

    def get_depot_coordinates(self) -> List[tuple]:
        if self.depots is None:
            raise ValueError('problem is not defined')
        return CoordinatesCollection.extract_coordinates(self.depots)

    def get_customers_coordinates(self) -> List[tuple]:
        if self.depots is None:
            raise ValueError('problem is not defined')
        return CoordinatesCollection.extract_coordinates(self.customers)

    @staticmethod
    def scaled_coordinate(c: Tuple[int, int], center: Tuple[int, int], width: int, height: int, m: int, factor=1.0):
        scaled_x = (((c[0] - center[0]) * 2 / m + 1) / 2 * width * factor) + (width * (1 - factor) / 2)
        scaled_y = (((c[1] - center[1]) * 2 / m + 1) / 2 * height * factor) + (height * (1 - factor) / 2)
        return int(scaled_x), int(scaled_y)

    def get_scaled_coordinates(self, width: int, height: int, factor=0.80):
        depot_coordinates = self.get_depot_coordinates()
        customer_coordinates = self.get_customers_coordinates()
        min_xy, max_xy = self.get_minmax_x_y(depot_coordinates + customer_coordinates)
        min_x, min_y = min_xy
        max_x, max_y = max_xy
        center = ((min_x + max_x)/2, (min_y + max_y)/2)
        m = max(max_x - min_x, max_y - min_y)
        scaled_depots = []
        scaled_customers = []
        for d in depot_coordinates:
            scaled_d = CoordinatesCollection.scaled_coordinate(d, center, width, height, m, factor)
            scaled_depots.append(scaled_d)
        for c in customer_coordinates:
            scaled_c = CoordinatesCollection.scaled_coordinate(c, center, width, height, m, factor)
            scaled_customers.append(scaled_c)
        return scaled_depots, scaled_customers


class MyCanvas(Canvas):
    def __init__(self, parent):
        super().__init__(parent)
        self.human = False
        self.config(background='white')
        self.bind("<Configure>", self.on_resize)
        self.bind('<Button-1>', self.on_click)

        self.problem: Union[SolomonsInstance, None] = None
        self.solution: Union[VrpSolution, None] = None

        self.collection = CoordinatesCollection()

        self.human_vrp: Union[MyCanvas.HumanInput, None] = None

    def set_problem(self, problem: SolomonsInstance):
        self.clear()
        self.problem = problem
        self.collection.set_problem(problem)
        self.redraw_canvas()

    def set_solution(self, solution: VrpSolution):
        self.solution = solution
        self.redraw_canvas()

    def redraw_canvas(self):
        self.delete('all')
        if self.problem:
            self.draw_problem()
        if self.solution:
            self.draw_solution()

    def on_resize(self, event):
        if self.human:
            return
        self.redraw_canvas()

    def on_click(self, event):
        coordinates = (event.x, event.y)
        if not self.human:
            return
        if not self.human_vrp.fleet:
            self.draw_depot(coordinates)
            depot_dialog = VehicleDialog()
            self.human_vrp.fleet = depot_dialog.get_vehicle_fleet()
            self.human_vrp.customers.append(Customer.create_customer(0, coordinates[0], coordinates[1], 0, 0, 900, 0))
        else:
            self.draw_customer(coordinates)
            CustomerDialog(coordinates[0], coordinates[1], 1, self.human_vrp.customers)

    def draw_depot(self, c: Tuple[int, int]):
        self.create_rectangle(c[0]-4, c[1]-4, c[0]+4, c[1]+4, fill='green')

    def draw_customer(self, c: Tuple[int, int]):
        self.create_oval(c[0] - 2, c[1] - 2, c[0] + 2, c[1] + 2, fill='red')

    def draw_problem(self):
        depots, customers = self.collection.get_scaled_coordinates(self.winfo_width(), self.winfo_height())
        for d in depots:
            self.draw_depot(d)
        for c in customers:
            self.draw_customer(c)

    def draw_solution(self):
        scaled_depots, scaled_customers = self.collection.get_scaled_coordinates(self.winfo_width(), self.winfo_height())
        routes = self.solution.routes
        customer_routes = [r.customers for r in routes]
        for customers in customer_routes:
            customer_indexes = [self.collection.get_customer_index(c) for c in customers]
            scaled_route = [scaled_customers[i] for i in customer_indexes]
            self.connect_mul_with_depot(scaled_depots, scaled_route, MyCanvas.random_color())

    def connect_dots(self, p1: tuple, p2: tuple, color: str):
        self.create_line(p1[0], p1[1], p2[0], p2[1], fill=color)

    def connect_mul_dots(self, dots: List[tuple], color: str):
        if len(dots) < 2:
            return
        last = dots[0]
        for i in range(1, len(dots)):
            current = dots[i]
            self.connect_dots(last, current, color)
            last = current

    def connect_mul_with_depot(self, depots: List[tuple], dots: List[tuple], color: str):
        if len(depots) == 0 or len(dots) == 0:
            return
        depot = depots[0]
        first = dots[0]
        last = dots[len(dots) - 1]
        self.connect_dots(first, depot, color)
        self.connect_dots(last, depot, color)
        self.connect_mul_dots(dots, color)

    def clear_problem(self):
        self.problem = None
        self.redraw_canvas()

    def clear_solution(self):
        self.solution = None
        self.redraw_canvas()

    def clear(self):
        self.solution = None
        self.problem = None
        self.redraw_canvas()

    def start_human(self):
        self.human = True
        self.clear()
        self.human_vrp = MyCanvas.HumanInput()

    def complete_human(self) -> Union[None, SolomonsInstance]:
        self.human = False
        if not self.human_vrp.fleet:
            return None
        vrp_problem = SolomonsInstance('Human Input', self.human_vrp.fleet, self.human_vrp.customers)
        return vrp_problem

    @staticmethod
    def random_color():
        return '#%02X%02X%02X' % (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))

    class HumanInput:
        def __init__(self):
            self.fleet: Union[VehicleFleet, None] = None
            self.customers: List[Customer] = []


class VrpGUI(Frame):
    def __init__(self, parent):
        super().__init__(parent)
        self.algorithms_collection = AlgorithmsCollection()
        self.simulator: Union[VrpSimulator, None] = None
        self.chosen_algorithm = StringVar(self)

        self.canvas = MyCanvas(self)
        self.menubar = Menu(self)
        self.human = Button(text="Human", relief="raised", command=self.toggle_button)
        self.ai_chooser = self.create_ai_chooser()
        self.statusbar = StatusBar(self)

        self.time_window_var = IntVar()

        self.add_menubar_commands()
        self.pack_components()

        self.problem: Union[SolomonsInstance, None] = None
        self.solution: Union[VrpSolution, None] = None

    def get_menubar(self):
        return self.menubar

    def create_ai_chooser(self) -> OptionMenu:
        choices = [self.algorithms_collection.get_text(k) for k in self.algorithms_collection.get_keys()]
        self.chosen_algorithm.set(choices[0])
        chooser = OptionMenu(self, self.chosen_algorithm, *choices)
        return chooser

    def add_menubar_commands(self):
        file_menu = Menu(self.menubar, tearoff=0)
        file_menu.add_command(label='New', command=self.clear)

        import_menu = Menu(self.menubar, tearoff=0)
        import_menu.add_command(label='Problem', command=self.import_problem)
        import_menu.add_command(label='Solution', command=self.import_solution)

        export_menu = Menu(self.menubar, tearoff=0)
        export_menu.add_command(label='Problem', command=self.export_problem)
        export_menu.add_command(label='Solution', command=self.export_solution)

        file_menu.add_cascade(label='Import', menu=import_menu)
        file_menu.add_cascade(label='Export', menu=export_menu)

        self.menubar.add_cascade(label="File", menu=file_menu)

    def pack_components(self):
        self.statusbar.pack(anchor='center')
        self.human.pack(anchor='ne')
        self.canvas.pack(side=TOP, fill=BOTH, expand=1)

        self.ai_chooser.pack(side=LEFT, anchor=W, fill=X)
        Checkbutton(self, text="time window", variable=self.time_window_var, command=self.on_time_window).pack(anchor=W, side=LEFT)
        solve_button = Button(self, text='solve', command=self.solve_algorithm)
        solve_button.pack(side=RIGHT, anchor=E)

    def toggle_button(self):
        if self.human.config('relief')[-1] == 'sunken':
            self.human.config(relief="raised")
            problem = self.canvas.complete_human()
            if problem is None:
                return
            self.set_problem(problem)
            messagebox.showinfo(title='Completed!', message='Problem is successfully loaded')
        else:
            self.human.config(relief="sunken")
            self.clear()
            self.canvas.start_human()

    def solve_algorithm(self):
        text = self.chosen_algorithm.get()
        algorithm_key = self.algorithms_collection.get_key_by_desc(text)
        if not self.problem:
            messagebox.showerror(title='Error', message='Problem is not loaded!')
        else:
            loading_dialog = LoadingDialog('Solving...')
            simulator = VrpSimulator(self.problem, time_window=self.get_time_window())
            solution = self.algorithms_collection.solve(algorithm_key, simulator)
            self.set_solution(solution)
            loading_dialog.quit()

    def clear_solution(self):
        self.solution = None
        self.canvas.clear_solution()
        self.refresh_fitness_data()

    def clear(self):
        self.solution = None
        self.problem = None
        self.canvas.clear()
        self.refresh_fitness_data()

    def set_problem(self, problem: SolomonsInstance):
        self.problem = problem
        self.simulator = VrpSimulator(problem)
        self.canvas.set_problem(problem)

    def set_solution(self, solution: VrpSolution):
        self.solution = solution
        self.canvas.set_solution(solution)
        self.refresh_fitness_data()

    def set_simplified_solution(self, simplified: List[List[int]]):
        solution = self.simulator.map_solution(simplified)
        self.set_solution(solution)

    def refresh_fitness_data(self):
        fitness = self.calc_fitness()
        if fitness is None:
            text = StatusBar.UNDEFINED
        else:
            text = StatusBar.DEFINED + '%.4f' % fitness
        self.statusbar.set_text(text)

    def calc_fitness(self) -> Union[None, float]:
        if not self.solution or not self.problem:
            return None
        if self.solution.fitness == 0.0:
            fitness = self.simulator.simulate(self.solution.simplify())
            self.solution.fitness = fitness
        return self.solution.fitness

    def get_time_window(self):
        return bool(self.time_window_var.get())

    def on_time_window(self):
        self.simulator.use_time_window(self.get_time_window())
        self.refresh_fitness_data()

    def import_problem(self):
        file_name = filedialog.askopenfilename(initialdir=FILES_FOLDER,
                                               title="Select problem file",
                                               filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        if file_name:
            problem_instance = read_vrp_instance(file_name)
            self.set_problem(problem_instance)

    def import_solution(self):
        file_name = filedialog.askopenfilename(initialdir=FILES_FOLDER,
                                               title="Select solution file")
        if file_name:
            simplified_solution = read_simplified_vrp_solution(file_name)
            self.set_simplified_solution(simplified_solution)

    def export_problem(self):
        if not self.problem:
            messagebox.showerror("Error", "Problem is not loaded!")
            return
        filename = filedialog.asksaveasfile(mode='w', defaultextension=".txt",
                                            initialdir=FILES_FOLDER, title='Export problem')
        if filename:
            write_vrp_instance(filename.name, 'w', self.problem)

    def export_solution(self):
        if not self.solution:
            messagebox.showerror("Error", "Solution is not loaded!")
            return
        filename = filedialog.asksaveasfile(mode='w', defaultextension=".txt",
                                            initialdir=FILES_FOLDER, title='Export solution')
        if filename:
            write_vrp_solution(filename.name, 'w', self.solution)


def start():
    window = Tk()
    window.title('VRP GUI')
    gui = VrpGUI(window)
    gui.pack(fill=BOTH, expand=1)
    window.config(menu=gui.get_menubar())
    window.mainloop()
