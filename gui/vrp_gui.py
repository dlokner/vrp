from tkinter import *
from tkinter import messagebox
from data.data_struct import *
import random
from tkinter import filedialog
from data.vrpIO import *
from simulator.vrp_simulator import VrpSimulator
from algorithms.genetic_algorithms import genetic_algorithm_permutation
from algorithms.genetic_algorithms import matrix_encoding
from algorithms import clarke_wright

clicked = 0
dictWrite = {
    "Clarke": "Clarke Wright Algorithm",
    "GA": "Genetic Algorithm"
}

dictAlg = {
    "Clarke": clarke_wright.ClarkWrightAlgorithm,
    "GA": matrix_encoding.GeneticAlgorithmMatrix
}

class VehicleDialog(Tk):

    def __init__(self):
        super().__init__()
        global clicked
        self.labels: List[Label] = []
        self.entries: List[Entry] = []

        self.labels.append(Label(self, text='Number of vehicles: ').grid(row=0, column=0))
        self.labels.append(Label(self, text='Capacity: ').grid(row=1, column=0))

        self.num = 0
        self.capacity = 0

        self.num_entry = Entry(self)
        self.capacity_entry = Entry(self)

        self.num_entry.grid(row=0, column=1)
        self.capacity_entry.grid(row=1, column=1)

        self.widgets = list(zip(self.labels, self.entries))
        self.submit1 = Button(self, text='Submit', command=self.on_submit)
        self.submit1.grid(row=2, column=1, sticky=E, padx=10, pady=10)

    def get_vehicle_fleet(self):
        return self.vehicle_fleet

    def on_submit(self):
        global clicked
        self.entries.append(self.num_entry)
        self.entries.append(self.capacity_entry)

        for entry in self.entries:
            if len(entry.get()) == 0:
                print(entry.get)
                messagebox.showerror("Error", "All entries must be filled.")
                self.vehicle_dialog = VehicleDialog()
                self.vehicle_dialog.title('Depot details')
                self.vehicle_dialog.geometry("250x80")
                self.destroy()
                break

        for entry in self.entries:
            if not entry.get().isdigit():
                messagebox.showerror("Error", entry.get() + " must be integer.")
                self.vehicle_dialog = VehicleDialog()
                self.vehicle_dialog.title('Depot details')
                self.vehicle_dialog.geometry("250x80")
                self.destroy()
                break

        clicked = 1
        self.num = int(self.num_entry.get())
        self.capacity = int(self.capacity_entry.get())
        self.vehicle_fleet = VehicleFleet(self.num, self.capacity)
        self.destroy()


class CustomerDialog(Tk):

    def __init__(self, x, y, num, list_customers):
        super().__init__()
        self.labels: List[Label] = []
        self.entries: List[Entry] = []

        self.x = x
        self.y = y
        self.num = num
        self.list_customers = list_customers

        self.labels.append(Label(self, text='Cargo: ').grid(row=1, column=0))
        self.labels.append(Label(self, text='Ready time: ').grid(row=2, column=0))
        self.labels.append(Label(self, text='Due date: ').grid(row=3, column=0))
        self.labels.append(Label(self, text='Service time: ').grid(row=4, column=0))

        self.cargo = 0
        self.ready_time = 0
        self.due_date = 0
        self.service_time = 0
        self.entries.clear()

        self.cargo_entry = Entry(self)
        self.ready_time_entry = Entry(self)
        self.due_date_entry = Entry(self)
        self.service_time_entry = Entry(self)

        self.cargo_entry.grid(row=1, column=1)
        self.ready_time_entry.grid(row=2, column=1)
        self.due_date_entry.grid(row=3, column=1)
        self.service_time_entry.grid(row=4, column=1)

        self.widgets = list(zip(self.labels, self.entries))
        self.submit = Button(self, text='Submit', command=self.on_submit)
        self.submit.grid(row=5, column=1, sticky=E, padx=10, pady=10)

    def on_submit(self):
        self.entries.append(self.cargo_entry)
        self.entries.append(self.ready_time_entry)
        self.entries.append(self.due_date_entry)
        self.entries.append(self.service_time_entry)

        for entry in self.entries:
            if len(entry.get()) == 0:
                messagebox.showerror("Error", "All entries must be filled.")
                self.customer_dialog = CustomerDialog(self.x, self.y, self.num, self.list_customers)
                self.customer_dialog.title('Customer details')
                self.customer_dialog.geometry("270x145")
                self.customer_dialog.resizable(width=True, height=True)

                self.destroy()
                break

        for i in range(1, len(self.entries)):
            if not self.entries[i].get().isdigit():
                messagebox.showerror("Error", self.entries[i].get() + " must be integer.")
                customer_dialog = CustomerDialog(self.x, self.y, self.num, self.list_customers)
                customer_dialog.title('Customer details')
                customer_dialog.geometry("270x145")
                customer_dialog.resizable(width=True, height=True)
                self.destroy()
                break

        self.cargo = int(self.cargo_entry.get())
        self.ready_time = int(self.ready_time_entry.get())
        self.due_date = int(self.due_date_entry.get())
        self.service_time = int(self.service_time_entry.get())

        self.customer = Customer.create_customer(self.num + 1, self.x, self.y, self.cargo, self.ready_time,
                                                 self.due_date,
                                                 self.service_time)
        self.list_customers.append(self.customer)
        self.destroy()

    def get_new_customer(self):
        return self.customar

    def get_customers(self):
        return self.list_customers


class MyCanvas(Frame):
    def __init__(self, parent, list_customers):
        super().__init__(parent)
        global clicked
        self.canvas = Canvas(self)
        self.canvas.config(background='white')
        self.canvas.bind('<Button-1>', self.click)
        self.canvas.pack(fill=BOTH, expand=True)
        self.list_customers = list_customers
        self.width = parent.winfo_screenwidth()
        self.height = parent.winfo_screenheight()

        self.num_customar = -1
        self.num_depot = 0
        self.depot: bool = True
        self.depots: List[tuple] = []
        self.customers: List[tuple] = []

    def add_depot(self, point: tuple):
        self.depots.append(point)

    def add_customer(self, point: tuple):
        self.customers.append(point)

    def get_depot_coordinates(self) -> List[tuple]:
        return self.depots

    def get_customers_coordinates(self) -> List[tuple]:
        return self.customers

    def connect_dots(self, p1: tuple, p2: tuple, color: str):
        self.canvas.create_line(p1[0], p1[1], p2[0], p2[1], fill=color)

    def connect_mul_dots(self, dots: List[tuple], color: str):
        if len(dots) < 2:
            return
        last = dots[0]
        for i in range(1, len(dots)):
            current = dots[i]
            self.connect_dots(last, current, color)
            last = current

    def connect_mul_with_depot(self, dots: List[tuple], color: str):
        if len(self.depots) == 0 or len(dots) == 0:
            return
        depot = self.depots[0]
        first = dots[0]
        last = dots[len(dots) - 1]
        self.connect_dots(first, depot, color)
        self.connect_dots(last, depot, color)
        self.connect_mul_dots(dots, color)

    def click(self, event):
        x, y = event.x, event.y
        global clicked
        if self.depot:
            self.canvas.create_rectangle(x - 7, y - 7, x + 7, y + 7, fill='red')
            self.add_depot((x, y))
            self.num_depot += 1
            self.depot = False
            self.vehicle_dialog = VehicleDialog()
            self.vehicle_dialog.geometry("250x80")
            self.vehicle_dialog.title('Depot details')

        else:
            self.num_customar += 1
            self.canvas.create_oval(x - 5, y - 5, x + 5, y + 5, fill='yellow')
            self.add_customer((x, y))
            self.customer_dialog = CustomerDialog(x, y, self.num_customar, self.list_customers)
            self.customer_dialog.title('Customer details')
            self.customer_dialog.geometry("270x145")
            self.customer_dialog.resizable(width=True, height=True)

    def get_vehicle_dialog(self):
        return self.vehicle_dialog

    def get_customer_dialog(self):
        return self.customer_dialog

    @staticmethod
    def max_xy(routes: List[List[int]], simulator):
        x = []
        y = []
        for route in routes:
            for i in range(0, len(route)):
                num = route[i]
                x.append(simulator.customer_mapper.get_customer_by_number(num).coordinate[0])
                y.append(simulator.customer_mapper.get_customer_by_number(num).coordinate[1])

        return max(x), max(y)

    @staticmethod
    def define_coordinates(simulator, num1: int, num2: int):
        x1 = simulator.customer_mapper.get_customer_by_number(num1).coordinate[0]
        y1 = simulator.customer_mapper.get_customer_by_number(num1).coordinate[1]
        x2 = simulator.customer_mapper.get_customer_by_number(num2).coordinate[0]
        y2 = simulator.customer_mapper.get_customer_by_number(num2).coordinate[1]

        return x1, y1, x2, y2

    def scale_and_draw(self, simulator, routes: List[List[int]]):
        max_x, max_y = self.max_xy(routes, simulator)
        a = self.width / max_x/ 1.7
        b = self.height / max_y/ 1.7

        for route in routes:
            r = lambda: random.randint(0, 255)
            colour = '#%02X%02X%02X' % (r(), r(), r())
            print(routes)
            if len(route) != 0:
                for i in range(0, len(route) - 1):
                    num = route[i]
                    num2 = route[i + 1]
                    x1, y1, x2, y2 = self.define_coordinates(simulator, num, num2)
                    x1 = x1+50
                    x2 = x2+50
                    y1 = y1+20
                    y2=  y2+20
                    self.canvas.create_oval(x1 * a - 2, y1 * b - 2, x1 * a + 2, y1 * b + 2, fill='red')
                    self.canvas.create_oval(x2 * a - 2, y2 * b - 2, x2 * a + 2, y2 * b + 2, fill='red')
                    self.canvas.create_line(x1 * a, y1 * b, x2 * a, y2 * b, fill=colour)

                    if i == 0:
                        x1, y1, x2, y2 = self.define_coordinates(simulator, num, 0)
                        x1 = x1 + 50
                        x2 = x2 + 50
                        y1 = y1 + 20
                        y2 = y2 + 20
                        self.canvas.create_line(x1 * a, y1 * b, x2 * a, y2 * b, fill=colour)
                        self.canvas.create_rectangle(x2 * a - 4, y2 * b - 4, x2 * a + 4, y2 * b + 4, fill='green')

                x1, y1, x2, y2 = self.define_coordinates(simulator, route[len(route) - 1], 0)
                x1 = x1 + 50
                x2 = x2 + 50
                y1 = y1 + 20
                y2 = y2 + 20
                self.canvas.create_line(x1 * a, y1 * b, x2 * a, y2 * b, fill=colour)

    def on_delete(self):
        self.depot = True
        self.list_customers.clear()
        self.canvas.delete('all')
        self.num_customar = -1
        self.num_depot = 0
        self.depot: bool = True
        self.depots: List[tuple] = []

    def on_import(self, button_new, window):
        self.on_delete()
        self.unbind(self.click)
        window.filename_problem = filedialog.askopenfilename(initialdir="/", title="Select problem file",
                                                             filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        print(window.filename_problem)

        window.filename_solution = filedialog.askopenfilename(initialdir="/", title="Select solution file",
                                                              filetypes=(("txt files", "*.txt"), ("all files", "*.*")))

        routes = read_simplified_vrp_solution(window.filename_solution)
        instance = read_vrp_instance(window.filename_problem)

        simulator = VrpSimulator(instance)

        self.scale_and_draw(simulator, routes)
        button_new.config(state=ACTIVE)

    def pressed(self, text, window):
        window.filename_problem = filedialog.askopenfilename(initialdir="/", title="Select problem file",
                                                             filetypes=(("txt files", "*.txt"), ("all files", "*.*")))
        instance = read_vrp_instance(window.filename_problem)
        simulator = VrpSimulator(instance)
        algorithm = dictAlg[text](simulator)
        solution = algorithm.solve()
        print(dictWrite[text])
        routes = []

        for route in solution.routes:
            r = []
            for x in route.customers:
                r.append(x.number)
            if len(r)!=0:
                routes.append(r)

        self.scale_and_draw(simulator, routes)

    def draw_human(self, simulator, routes):
        for route in routes:
            r = lambda: random.randint(0, 255)
            colour = '#%02X%02X%02X' % (r(), r(), r())
            if len(route) != 0:
                for i in range(0, len(route) - 1):
                    num = route[i]
                    num2 = route[i + 1]
                    x1, y1, x2, y2 = self.define_coordinates(simulator, num, num2)
                    self.canvas.create_line(x1, y1, x2, y2, fill=colour)

                    if i == 0:
                        x1, y1, x2, y2 = self.define_coordinates(simulator, num, 0)
                        self.canvas.create_line(x1, y1, x2, y2, fill=colour)

                x1, y1, x2, y2 = self.define_coordinates(simulator, route[len(route) - 1], 0)
                self.canvas.create_line(x1, y1, x2, y2, fill=colour)

    def human_draw(self, text, simulator):
        algorithm = dictAlg[text](simulator)
        solution = algorithm.solve()
        print(dictWrite[text])
        routes = []
        for route in solution.routes:
            r = []
            for x in route.customers:
                r.append(x.number)
            routes.append(r)
        self.draw_human(simulator, routes)




def main():
    def click():
        canvas.connect_mul_with_depot(canvas.customers, '#00aabb')

    def on_done():
        window.destroy()

    def on_import():
        button_new.config(state=DISABLED)
        canvas.on_import(button_new, window)

    def on_export_problem():

        list_customers = canvas.get_customer_dialog().get_customers()
        depot_coordinates = canvas.get_depot_coordinates()
        list_customers.insert(0,
                              Customer.create_customer(0, depot_coordinates[0][0], depot_coordinates[0][1], 0, 0, 0, 0))
        vehicle_fleet = canvas.get_vehicle_dialog().get_vehicle_fleet()
        instance = SolomonsInstance('Human solomun', vehicle_fleet, list_customers)
        write_vrp_instance('Human instance', 'w', instance)
        simulator = VrpSimulator(instance)
        ai_window = Toplevel(window)
        ai_window.title('Choose algorithm')

        def var_states():
            if var1.get() == 1:
                var = 'GA'
            else:
                var = 'Clarke'
            canvas.human_draw(var, simulator)

        var1 = IntVar()
        Checkbutton(ai_window, text="Genetic Algorithm", variable=var1).grid(row=1, sticky=W)
        var2 = IntVar()
        Checkbutton(ai_window, text="Clarke Wright Algorithm", variable=var2).grid(row=2, sticky=W)
        Button(ai_window, text='Quit', command=ai_window.quit).grid(row=3, sticky=W, pady=4)
        Button(ai_window, text='OK', command=var_states).grid(row=4, sticky=W, pady=4)


    def on_new():
        canvas.on_delete()

    def pressed(text):
        canvas.pressed(text, window)

    def on_ai():
        ai_window = Toplevel(window)
        ai_window.title('Choose algorithm')

        def var_states():
            if var1.get() == 1:
                var = 'GA'
            else:
                var = 'Clarke'
            pressed(var)

        var1 = IntVar()
        Checkbutton(ai_window, text="Genetic Algorithm", variable=var1).grid(row=1, sticky=W)
        var2 = IntVar()
        Checkbutton(ai_window, text="Clarke Wright Algorithm", variable=var2).grid(row=2, sticky=W)
        Button(ai_window, text='Quit', command=ai_window.quit).grid(row=3, sticky=W, pady=4)
        Button(ai_window, text='OK', command=var_states).grid(row=4, sticky=W, pady=4)

    window = Tk()
    window.title('VRP GUI')
    window.grid()
    window.attributes('-fullscreen', True)
    list_customers: List[Customer] = []
    canvas = MyCanvas(window, list_customers)
    canvas.pack(fill=BOTH, expand=True)

    button_import = Button(window, text='Import', command=on_import)
    button_import.pack(side=LEFT)
    button_export_problem = Button(window, text='Export problem', command=on_export_problem, state=ACTIVE)
    button_export_problem.pack(side=LEFT)
    button_new = Button(window, text='New', command=on_new)
    button_new.pack(side=LEFT)
    button_ai = Button(window, text='AI', command=on_ai, state=ACTIVE)
    button_ai.pack(side=LEFT)
    button = Button(window, text='Done', command=on_done, state=ACTIVE)
    button.pack(side=BOTTOM, fill=BOTH)

    click()

    window.mainloop()


if __name__ == "__main__":
    main()
